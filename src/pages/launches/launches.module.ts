import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaunchesPage } from './launches';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    LaunchesPage,
  ],
  imports: [
    IonicPageModule.forChild(LaunchesPage),
    IonicImageViewerModule
  ],
})
export class LaunchesPageModule {}
