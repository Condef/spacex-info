import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';

@IonicPage()
@Component({
  selector: 'page-launches',
  templateUrl: 'launches.html',
  animations: [
    trigger('itemState', [
      transition('* => *', [ 
        query(':leave', [
          stagger(50, [
            animate('0.5s ease-out', style({ left: '-50%',opacity: 0 }))
          ])
        ], { optional: true,
             limit: 15 }),
        query(':enter', [
          style({ left: '-50%', opacity: 0 }),
          stagger(10, [
            animate('0.5s ease-out', style({ left: '0',opacity: 1 }))
          ])
        ], { optional: true,
             limit: 15 })
      ])
    ])
  ]
})
export class LaunchesPage {
  launches
  loading = true
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider
  ) { }

  ionViewDidLoad() {
    this.api.getAllLaunches().then(allLaunches => {
      this.launches = allLaunches
      this.loading = false
      console.log(this.launches)
    }).catch(err => {
      this.ionViewDidLoad()
    })
  }

  openLaunchDetails(launch) {
    this.navCtrl.push('LaunchInfoPage', { launch: launch })
  }

}
