import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';

@IonicPage()
@Component({
  selector: 'page-capsules',
  templateUrl: 'capsules.html',
  animations: [
    trigger('itemState', [
      transition('* => *', [ 
        query(':leave', [
          stagger(100, [
            animate('0.5s ease-out', style({ opacity: 0, transform: 'scale(0.5) translateY(200px)' }))
          ])
        ], { optional: true,
             limit: 4 }),
        query(':enter', [
          style({ opacity: 0, transform: 'scale(0.2) translateY(200px)' }),
          stagger(100, [
            animate('0.5s ease-out', style({ bottom: '0',opacity: 1, transform: 'scale(1) translateY(0)' }))
          ])
        ], { optional: true,
             limit: 4 })
      ])
    ])
  ]
})
export class CapsulesPage {
  loading = true
  capsules
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController
  ) { }

  ionViewDidLoad() {
    this.api.getAllCapsules().then(allCapsules => {
      this.capsules = allCapsules
      this.loading = false
      console.table(this.capsules)
    }).catch(err => {
      this.ionViewDidLoad()
    })
  }

}
