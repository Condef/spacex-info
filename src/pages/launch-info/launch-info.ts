import { Component,ChangeDetectorRef, ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-launch-info',
  templateUrl: 'launch-info.html',
})
export class LaunchInfoPage {
  launch
  showToolbar:boolean = false;
  showBlend = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public ref: ChangeDetectorRef
  ) {
    this.launch = navParams.get("launch")
    console.log(this.launch);
  }

  ionViewDidLoad() {

  }

  onScroll($event: any){
    let scrollTop = $event.scrollTop;
    this.showToolbar = scrollTop >= 80;
    this.ref.detectChanges();
  }

  openLaunchSite(site_id) {
    this.navCtrl.push('LaunchSiteInfoPage', {launchSiteID: site_id})
  }

  openWeb(link) {
    open(link, "_system")
  }

  openFab(fab) {
    this.showBlend = true;
    setTimeout(() => {
      let el = document.querySelector('div.blend')
      el ? el.addEventListener('click', () => {
        this.showBlend = false;
        fab.close();
      }) : null
    }, 200);
  }
}
