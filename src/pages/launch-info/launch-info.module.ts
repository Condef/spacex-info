import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaunchInfoPage } from './launch-info';
import { IonicImageViewerModule } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    LaunchInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(LaunchInfoPage),
    IonicImageViewerModule
  ],
})
export class LaunchInfoPageModule {}
