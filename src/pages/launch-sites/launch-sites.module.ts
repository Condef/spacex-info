import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaunchSitesPage } from './launch-sites';

@NgModule({
  declarations: [
    LaunchSitesPage,
  ],
  imports: [
    IonicPageModule.forChild(LaunchSitesPage),
  ],
})
export class LaunchSitesPageModule {}
