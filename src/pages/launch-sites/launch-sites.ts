import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { trigger, transition, style, animate, query, stagger } from '@angular/animations';

@IonicPage()
@Component({
  selector: 'page-launch-sites',
  templateUrl: 'launch-sites.html',
  animations: [
    trigger('itemState', [
      transition('* => *', [ 
        query(':leave', [
          stagger(50, [
            animate('0.3s ease-out', style({ opacity: 0, transform: 'scale(0.5) translateY(200px)' }))
          ])
        ], { optional: true,
             limit: 3 }),
        query(':enter', [
          style({ opacity: 0, transform: 'scale(0.2) translateY(200px)' }),
          stagger(50, [
            animate('0.3s ease-out', style({ bottom: '0',opacity: 1, transform: 'scale(1) translateY(0)' }))
          ])
        ], { optional: true,
             limit: 3 })
      ])
    ])
  ]
})
export class LaunchSitesPage {
  allLaunchPads
  loading = true
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider
  ) {
  }

  ionViewDidLoad() {
    this.api.getAllLaunchPads().then(launchSite => {
      this.allLaunchPads = launchSite
      this.loading = false
      console.table(this.allLaunchPads)
    }).catch(err => {
      this.ionViewDidLoad()
    })
  }

  // TODO: Export to config.
  openMaps(lat, long) {
    open(`https://www.google.pl/maps/@${long},${lat},15.25z`, "_system")
  }

  openDetails(launchpad) {
    this.navCtrl.push('LaunchSiteInfoPage', {launchSite: launchpad})
  }

}
