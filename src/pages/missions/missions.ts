import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Content } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { trigger, transition, query, stagger, animate, style } from '@angular/animations';

@IonicPage()
@Component({
  selector: 'page-missions',
  templateUrl: 'missions.html',
  animations: [
    trigger('fadeIN', [
      transition(':leave', [
        style({ opacity: 1 }),
        animate('0.5s', style({ opacity: 0 }))
      ]),
        transition(':enter', [
          style({ opacity: 0 }),
          animate('0.5s', style({ opacity: 1 }))
      ])
    ])
  ],
})
export class MissionsPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;
  missions
  loading = true
  activeCardIndex = 0
  cardVisible = false
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider
  ) { }

  ionViewDidLoad() {
    this.api.getAllMissions().then(allMissions => {
      this.missions = allMissions
      this.loading = false
      this.cardVisible = true
      console.table(this.missions)
    }).catch(err => {
      this.ionViewDidLoad()
    })
  }

  slideChanged(ev) {
    this.content.scrollToTop()
    let temp = this.slides.getActiveIndex()
    if (temp < this.missions.length) this.activeCardIndex = temp
    this.cardVisible = false
    setTimeout(() => {
      this.cardVisible = true
    }, 0)
  }

  openWeb(url) {
    open(url, "_system")
  }

}
