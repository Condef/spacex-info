import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-launch-site-info',
  templateUrl: 'launch-site-info.html',
})
export class LaunchSiteInfoPage {
  launchSiteID
  launchSite
  loading = true
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider
  ) {
  }
  ionViewDidLoad() {
    this.launchSiteID = this.navParams.get('launchSiteID')
    if (this.launchSiteID) {
      this.api.getOneLaunchPad(this.launchSiteID).then(launchSite => {
        this.launchSite = launchSite
        this.loading = false
        console.log(this.launchSite)
      }).catch(err => {
        this.ionViewDidLoad()
      })
    } else {
      this.launchSite = this.navParams.get('launchSite')
      this.loading = false
    }
    console.log(this.launchSiteID || this.launchSite)
  }

  openWeb(link) {
    open(link, "_system")
  }
}
