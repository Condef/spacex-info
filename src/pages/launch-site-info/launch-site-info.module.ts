import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaunchSiteInfoPage } from './launch-site-info';

@NgModule({
  declarations: [
    LaunchSiteInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(LaunchSiteInfoPage),
  ],
})
export class LaunchSiteInfoPageModule {}
