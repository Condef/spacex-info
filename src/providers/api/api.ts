import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../../configs/config';

@Injectable()
export class ApiProvider {

  constructor(
    public http: HttpClient
  ) { }

  getAllCapsules() {
    return this.http.get(Config.apiUrl + 'capsules').toPromise()
  }

  getOneCapsule(capsule_serial) {
    return this.http.get(Config.apiUrl + 'capsules/' + capsule_serial).toPromise()
  }

  getUpcomingCapsules() {
    return this.http.get(Config.apiUrl + 'capsules/upcoming').toPromise()
  }

  getPastCapsules() {
    return this.http.get(Config.apiUrl + 'capsules/past').toPromise()
  }

  getAllCores() {
    return this.http.get(Config.apiUrl + 'cores').toPromise()
  }
  getOneCore(core_serial) {
    return this.http.get(Config.apiUrl + 'cores/' + core_serial).toPromise()
  }

  getUpcomingCores() {
    return this.http.get(Config.apiUrl + 'cores/upcoming').toPromise()
  }

  getPastCores() {
    return this.http.get(Config.apiUrl + 'cores/past').toPromise()
  }

  getAllDragons() {
    return this.http.get(Config.apiUrl + 'dragons').toPromise()
  }
  getOneDragon(id) {
    return this.http.get(Config.apiUrl + 'dragons/' + id).toPromise()
  }

  getCompanyInfo() {
    return this.http.get(Config.apiUrl + 'info').toPromise()
  }

  getApiInfo() {
    return this.http.get(Config.apiUrl + '').toPromise()
  }

  getAllLandingPads() {
    return this.http.get(Config.apiUrl + 'landpads').toPromise()
  }

  getOneLandingPad(id) {
    return this.http.get(Config.apiUrl + 'landpads/' + id).toPromise()
  }

  getAllLaunches() {
    return this.http.get(Config.apiUrl + 'launches').toPromise()
  }
  
  getOneLaunch(flight_number) {
    return this.http.get(Config.apiUrl + 'launches/' + flight_number).toPromise()
  }
  
  getPastLaunches() {
    return this.http.get(Config.apiUrl + 'launches/past').toPromise()
  }

  getUpcomingLaunches() {
    return this.http.get(Config.apiUrl + 'launches/upcoming').toPromise()
  }

  getLatestLaunch() {
    return this.http.get(Config.apiUrl + 'launches/latest').toPromise()
  }

  getNextLaunch() {
    return this.http.get(Config.apiUrl + 'launches/next').toPromise()
  }

  getAllLaunchPads() {
    return this.http.get(Config.apiUrl + 'launchpads').toPromise()
  }
  
  getOneLaunchPad(site_id) {
    return this.http.get(Config.apiUrl + 'launchpads/' + site_id).toPromise()
  }
  
  getAllMissions() {
    return this.http.get(Config.apiUrl + 'missions').toPromise()
  }

  getOneMission(mission_id) {
    return this.http.get(Config.apiUrl + 'missions/' + mission_id).toPromise()
  }

  getAllPayloads() {
    return this.http.get(Config.apiUrl + 'payloads').toPromise()
  }

  getOnePayload(payload_id) {
    return this.http.get(Config.apiUrl + 'payloads/' + payload_id).toPromise()
  }

  getAllRockets() {
    return this.http.get(Config.apiUrl + 'rockets').toPromise()
  }

  getOneRocket(rocket_id) {
    return this.http.get(Config.apiUrl + 'rockets/' + rocket_id).toPromise()
  }

  getRoadsterInfo() {
    return this.http.get(Config.apiUrl + 'roadster').toPromise()
  }

  getAllShips() {
    return this.http.get(Config.apiUrl + 'ships').toPromise()
  }

  getOneShip(ship_id) {
    return this.http.get(Config.apiUrl + 'ships/' + ship_id).toPromise()
  }

}
